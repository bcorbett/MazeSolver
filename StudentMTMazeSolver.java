package cmsc433.p3;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.*;

import cmsc433.p3.SkippingMazeSolver.SolutionFound;
import cmsc433.p3.*;

/**
 * This file needs to hold your solver to be tested. 
 * You can alter the class to extend any class that extends MazeSolver.
 * It must have a constructor that takes in a Maze.
 * It must have a solve() method that returns the datatype List<Direction>
 *   which will either be a reference to a list of steps to take or will
 *   be null if the maze cannot be solved.
 */
public class StudentMTMazeSolver extends SkippingMazeSolver
{
	List<Direction> result = null;
	List<Direction> solution;
	private ForkJoinPool fjPool;
	private int THRESHOLD;
	LinkedList<Position> node;
	LinkedList<Choice> choiceStack = new LinkedList<Choice>();
	Choice ch;
	RecursiveAction task;
	int b;
	
    public StudentMTMazeSolver(Maze maze)
    {
        super(maze);
    }
    
    @SuppressWarnings("serial")
	private class MTMSTask extends RecursiveAction {
		
		  // Elements to be sorted
		Choice head;
		Direction d;
		public MTMSTask(Choice head, Direction d) {
			this.head = head;
			this.d = d;
		}
		
		
		   public void compute () {
			   LinkedList<Choice> choiceStack = new LinkedList<Choice>();
			   Choice curr;
					try{
						choiceStack.push(this.head);
						while(!choiceStack.isEmpty()){
							curr = choiceStack.peek();
							if(curr.isDeadend()){
								choiceStack.pop();
								if(!choiceStack.isEmpty()) choiceStack.peek().choices.pop();
									continue;	
								
							}
							choiceStack.push(follow(curr.at, curr.choices.peek()));
							}
						result = null;
						
					}catch(SolutionFound s){
						Iterator<Choice> iter = choiceStack.iterator();
						LinkedList<Direction> solutionPath = new LinkedList<Direction>();
						while (iter.hasNext()){
							curr = iter.next();
							solutionPath.push(curr.choices.peek());
						}
						solutionPath.push(d);
						if(maze.display != null) maze.display.updateDisplay();
							 result = pathToFullPath(solutionPath);
							 solution = result;
						}
				}	
		   

    }
    
  
    
    @SuppressWarnings("unchecked")
	public List<Direction> solve()
    {
    	int cpu = Runtime.getRuntime().availableProcessors();
    	fjPool = new ForkJoinPool();
    	try{
    		Choice begin = firstChoice(maze.getStart());
    		int size = begin.choices.size();
    		b = size;
    		THRESHOLD = size / cpu;
    		for(int i = 0; i< size; i++){
    			Choice curr = follow(begin.at, begin.choices.peek());
    			task = new MTMSTask(curr,begin.choices.pop()); 
    			fjPool.execute(task); 
    			task.join();
    		}
    		
    		
    	}catch(SolutionFound s){
    		
    		
    	}
    	
    	fjPool.shutdown();
    	return solution; 
         }
    
}
 
        // TODO: Implement your code here
   
    
    
   
    	

    

